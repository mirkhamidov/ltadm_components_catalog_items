<?
$JS_id_lt_catalog_chapters=<<<END
  var els=\$('*[name="{FORM_NAME}"] option:selected').attr('changeValue');
  els=explode(',',els);
  \$('table[idf="item{KEY_FIELD}"] tr.PropertyString').hide();
  if (els!=null){
	for (var i=0;i<els.length;i++){
	  \$('table[idf="item{KEY_FIELD}"] tr.PropertyString[rel="'+els[i]+'"]').show();
	}
  }
END;

$items=array(
  "id_lt_catalog_chapters" => array(
		 "desc" => "Раздел каталога",
		 "type" => "select_from_tree_table",
		 "table" => "lt_catalog_chapters",
		 "key_field" => "id_lt_catalog_chapters",
		 "parent_field" => "parent",
		 "fields" => array("name","propertys_list"),
		 "show_field" => "%1",
		 "condition" => "",
		 #"is_root" => true,
		 #"root_desc" => "Корневой раздел",
		 "order" => array ("orders" => "ASC"),
		 "changeByValue" => array(
		   "field" => "propertys_list",
		   "javascript" => $JS_id_lt_catalog_chapters,
		 ),
	   ),


  "name" => array(
		 "desc" => "Название товара",
		 "type" => "text",
		 "maxlength" => "255",
		 "size" => "70",
		 "select_on_edit" => true,
		 "js_validation" => array(
		   "js_not_empty" => "Поле должно быть не пустым!",
		 ),
	   ),
  "artikul" => array(
		 "desc" => "Артикул",
		 "type" => "text",
		 "maxlength" => "255",
		 "size" => "70",
		 "select_on_edit" => true,
	   ),

	  "photo" => array
	  (
		"type"                  => "processed_image",
		"file_is"               => "processed_image",
		"desc"                  => "Фотография",
		"full_desc"             => "Только изображения форматов JPEG, GIF и PNG",
		"store_source_name"     => true,          // сохранять ли имя загруженного файла, которое тот имеет на компьютере пользователя?
		"store_dimensions"      => true,          // хранить ли в БД размеры картинки, или определять их из файла (false)
		"check_file"            => true,          // проверять ли при отображении картинки существование файла, или доверять информации из БД (false)
		"check_dimensions"      => true,          // проверять ли при отображении картинки её размеры, или доверять информации из БД (false)
		"lt_show_system"        => true,          // разрешает отображать lt_source и lt_preview
		"lt_delete_files"       => true,          // разрешает возможность удалять файлы картинок для данной записи
		"lt_allow_name_change"  => false,          // разрешает загружать файл под именем, указанным пользователем
		"lt_preview"            => "preview",        // индекс картинки, которая считается картинкой для предварительного просмотра
		"lt_allow_remote_files" => true,

		"images" => array
		(
		  "preview" => array
		  (
			"prefix"  => "small_",
			"target_type" => 2,
			"transforms" => array
			(
			  "resize" => array(
					"w"=> 100,
					#"h"=>85,
					"backgroundColor" => array(255,255,255)
			  ),
			),
			"quality" => 100
		  ),
		  "show" => array
		  (
			"prefix"  => "show_",
			"target_type" => 2,
			"transforms" => array
			(
				"resize" => array(
					"w"=> 500,
					#"h"=>0,
					"backgroundColor" => array(255,255,255)
				),
			),
			"quality" => 100
		  ),

		  "source" => array
		  (
			"prefix"  => "src_",
			"target_type" => 2,
			"quality" => 100
		  ),


		),
		"select_on_edit" => true,
	  ),
	"short_content" => array(
		"desc" => "Краткое описание",
		"type" => "textarea",
		"width" => "70",
		"height" => "5",
		"select_on_edit" => true,
	),

  "content" => array(
		 "desc" => "Описание товара",
		 "type" => "editor",
		 "width" => "700",
		 "height" => "500",
		 "select_on_edit" => true,
	   ),
	
	'is_sized' => array(
		"desc" => "С размером?",
		"type" => "radio",
		"select_desc" => "",
		"full_desc" => "",
		"values" => array (0=>'Нет',1=>'Да'),
		"switch" => array (
			0 => array(
				enabled => array(),
				disabled => array('sizes'),
			),
			1 => array(
				enabled => array('sizes'),
				disabled => array(),
			),
		),
		"default_value" => 0,
		"select_on_edit" => true,
		"select_on_edit_disabled" => true,
	),
	
	'sizes' => array(
		'not_in_table' => true,
		'desc' => 'Размеры к товару',
		'type' => 'size2height',
		'cross_tbl' => 'lt_good_size2height',
		'good_key' => 'id_lt_catalog_items',
		'params' => array(
			'sizes' => array(
				'table' => 'lt_good_sizes',
				'val' => 'size',
			),
			'heights' => array(
				'table' => 'lt_good_heights',
				'val' => 'height',
			),
		),
	),

  "price" => array(
		 "desc" => "Цена",
		 "style" => "text-align:right",
		 "type" => "text",
		 "maxlength" => "10",
		 "size" => "7",
		 "select_on_edit" => true,
		 "desc_after" => "руб.",
		 "js_validation" => array(
		   "js_match" => array (
			 "pattern" => "^[\d|.]+$",
			 "flags" => "g",
			 "error" => "Только цифры! Дробные числа отделяются точкой",
		   ),
		 ),
	   ),

);
?>