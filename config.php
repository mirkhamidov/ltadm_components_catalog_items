<?
$config=array(
  "name" => "Товары каталога",
  "status" => "system",
  "windows" => array(
                      "create" => array("width" => 1000,"height" => 700),
                      "edit" => array("width" => 550,"height" => 400),
                       "list" => array("width" => 800,"height" => 500),
                    ),
  "right" => array("admin","#GRANTED"),
  "main_table" => "lt_catalog_items",
  "list" => array(
    "name" => array("isLink" => true),
    // "photo" => array("isLink" => true,"disabledSort" => true),
    "id_lt_catalog_chapters" => array("disabledSort" => true),
  ),
  "select" => array(
     "max_perpage" => 10,
     "default_orders" => array(
                           array("name" => "ASC"),
                         ),
     "default" => array(
        "id_lt_catalog_chapters" => array(
               "desc" => "Категория каталога",
               "type" => "select_from_tree_table",
               "table" => "lt_catalog_chapters",
               "key_field" => "id_lt_catalog_chapters",
               "parent_field" => "parent",
               "fields" => array("name"),
               "show_field" => "%1",
               "condition" => "",
               "use_empty" => true,
               "in_list" => true,
               "is_root" => true,
               "empty_desc" => "&#187; Не важно",
               "root_desc" => "&#187; Корневой раздел",
               "order" => array ("orders" => "ASC","name" => "ASC"),
             ),

     ),
  ),
);

$actions=array(
	"create" => array(
		#"before_code" => "create.php",
	),
	"edit" => array(
		"before_code" => "edit.php",
	),
);

?>